# YoutubeChannel

This module creates a youtube channel in a block for the website. With a given
youtube channel username you can access the latest videos of the user. You can
also set the height and width required for the youtube channel.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/youtubechannel)

- To submit bug reports and feature suggestions or to track changes
  [issue queue](https://www.drupal.org/project/issues/youtubechannel)


## Requirements

This module requires no modules outside of Drupal core.


## Installation

1. Extract the tar.gz into your 'modules' directory.
2. Enable the module at 'admin/modules'.


## Configuration

- Install the youtubechannel module
- Grab an API key.
  ([details here](https://www.drupal.org/node/2474977#comment-9900267))
- Grab the channel ID from your youtube channel (open the page and look at the
  URL).
- Configure the module and add the settings above from
  `admin/config/services/youtubechannel`.
- Enable the youtubechannel block.


## Maintainers

- [josueggh](https://www.drupal.org/user/1149200)
- [Parwan Bisht (parwan005)](https://www.drupal.org/user/2046786)
- [Nitin Patil (NitinSP)](https://www.drupal.org/user/2198938)
